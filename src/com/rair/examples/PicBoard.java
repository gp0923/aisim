package com.rair.examples;

import javax.swing.*;
import java.awt.*;

/**
 * Created by gavin on 6/22/17.
 */
public class PicBoard extends JPanel {

    private Image bardejov;

    public PicBoard() {

        initBoard();
    }

    private void initBoard() {

        loadImage();

        int w = bardejov.getWidth(this);
        int h =  bardejov.getHeight(this);
        setPreferredSize(new Dimension(w, h));
    }

    private void loadImage() {

        ImageIcon ii = new ImageIcon("resources/images/star.png");
        bardejov = ii.getImage();
    }

    @Override
    public void paintComponent(Graphics g) {

        g.drawImage(bardejov, 0, 0, null);
    }
}
