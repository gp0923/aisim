package com.rair.examples;

import javax.swing.*;
import java.awt.*;

/**
 * Created by gavin on 6/22/17.
 * Controls the main application
 */
public class Application extends JFrame {

    private Application() {

        initUI();
    }

    private void initUI() {

        add(new PicBoard());

        //setSize(300, 300);
        pack();

        setTitle("Donut");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            Application ex = new Application();
            ex.setVisible(true);
        });
    }
}