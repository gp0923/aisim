package com.rair.examples.sokoban;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Wall extends Actor {

    private Image image;

    public Wall(int x, int y) {
        super(x, y);

        //URL loc = this.getClass().getResource("/AISim/resources/images/sokoban/wall.png");
        ImageIcon iia = new ImageIcon("resources/images/sokoban/wall.png");
        image = iia.getImage();
        this.setImage(image);

    }
}