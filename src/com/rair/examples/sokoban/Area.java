package com.rair.examples.sokoban;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Area extends Actor {

    public Area(int x, int y) {
        super(x, y);

        //URL loc = this.getClass().getResource("resources/images/sokoban/area.png");
        ImageIcon iia = new ImageIcon("resources/images/sokoban/area.png");
        Image image = iia.getImage();
        this.setImage(image);
    }
}