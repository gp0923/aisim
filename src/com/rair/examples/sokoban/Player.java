package com.rair.examples.sokoban;

import javax.swing.*;
import java.awt.*;

public class Player extends Actor {

    public Player(int x, int y) {
        super(x, y);

        //URL loc = this.getClass().getResource("/resources/images/sokoban/sokoban.png");
        ImageIcon iia = new ImageIcon("resources/images/sokoban/sokoban.png");
        Image image = iia.getImage();
        this.setImage(image);
    }

    public void move(int x, int y) {
        int nx = this.x() + x;
        int ny = this.y() + y;
        this.setX(nx);
        this.setY(ny);
    }
}