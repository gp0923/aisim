package com.rair.simulator;

import java.awt.*;
import java.util.logging.Level;

/**
 * Contains universal constants for the AI simulator
 */
public interface Commons {

    int BOARD_COLUMNS = 16;
    int BOARD_ROWS = 12;

    /**
     *  Size of the edge of a grid square in pixels.
     */
    int CELL_LENGTH = 48;
    /**
     * The maximum vertical size that sprites will be displayed.
     * Counted in pixels.
     */
    int SPRITE_HEIGHT = 32;

    /**
     * The maximum horizontal size that sprites will be displayed.
     * Counted in pixels.
     */
    int SPRITE_WIDTH = 32;

    /**
     * The delay (in ms) between simulator updates
     */
    int DELAY = 20;

    /**
     * The amount of simulation update ticks that occur each second
     */
    int TICKS_PER_SECOND = 1000 / DELAY;

    /**
     * The color that cells are highlighted when selected
     */
    Color HIGHLIGHT_COLOR = Color.orange;

    int CENTER = 0;
    int RIGHT = 1;
    int DOWN = 2;
    int LEFT = 3;
    int UP = 4;

    Level LOG_LEVEL = Level.FINE;

}
