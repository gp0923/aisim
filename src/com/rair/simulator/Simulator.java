package com.rair.simulator;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Created by gavin on 6/26/17.
 */
public class Simulator extends JFrame implements Commons {
    protected static final Logger LOGGER = Logger.getLogger( Simulator.class.getName() );
    public Simulator (){
        initUI();
    }

    /**
     * Initializes the board
     */
    private void initUI() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }
        add(new Board());
        setTitle("AI simulator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(BOARD_COLUMNS * CELL_LENGTH, BOARD_ROWS * CELL_LENGTH);
        setLocationRelativeTo(null);
        setResizable(false);
        pack();
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            LOGGER.setLevel(LOG_LEVEL);
            Logger log = LogManager.getLogManager().getLogger("");
            for (Handler h : log.getHandlers()) {
                h.setLevel(LOG_LEVEL);
            }
            LOGGER.log(Level.FINE, "Creating simulator");
            Simulator sim = new Simulator();
            LOGGER.log(Level.FINE, "simulator Created, setting visible");
            sim.setVisible(true);
            LOGGER.log(Level.FINE, "Successfully set visible");
        });
    }

}
