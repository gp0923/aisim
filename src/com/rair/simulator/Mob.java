package com.rair.simulator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by gavin on 6/27/17.
 */
public class Mob extends Sprite {
    protected Image[] images;
    protected int direction;
    private boolean moving;
    private boolean moveOnce;
    private ReentrantLock moveLock;

    public Mob (int row, int col, Board board){
        super(row, col, board);
        direction = CENTER;
        moving = false;
        moveOnce =false;
        moveLock = new ReentrantLock();
        stop();
    }
    @Override
    protected void initImages(){
        images = new Image[5];
        loadImage("resources/simulator/mob.png", CENTER);
        loadImage("resources/simulator/mob_right.png", RIGHT);
        loadImage("resources/simulator/mob_down.png", DOWN);
        loadImage("resources/simulator/mob_left.png", LEFT);
        loadImage("resources/simulator/mob_up.png", UP);
        setImage(images[CENTER]);
    }

    public void loadImage(String imageName, int direction) {

        ImageIcon ii = new ImageIcon(imageName);
        setImage(ii.getImage(), direction);
    }

    public void setImage(Image image, int direction) {
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        if (w > SPRITE_WIDTH || h > SPRITE_HEIGHT){
            double ratio = Math.min(SPRITE_WIDTH/(double) w,
                    SPRITE_HEIGHT/(double) h);
            int width = (int) (w * ratio);
            int height = (int) (h * ratio);
            images[direction] = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        }
        else{
            images[direction] = image;
        }

    }

    @Override
    public void act(){

        if (moving){
            switch (direction){
                case RIGHT:
                    board.moveSprite(this, getRow(), getCol()  + 1);
                    break;
                case DOWN:
                    board.moveSprite(this, getRow()  + 1, getCol());
                    break;
                case LEFT:
                    board.moveSprite(this, getRow() , getCol() - 1);
                    break;
                case UP:
                    board.moveSprite(this, getRow() - 1, getCol());
                    break;
            }
        }
        if (moveOnce)
            stop();

    }

    @Override
    public void keyPressed(KeyEvent e){
        int code = e.getKeyCode();
        switch (code){
            case KeyEvent.VK_RIGHT:
                move(RIGHT);
                break;
            case KeyEvent.VK_DOWN:
                move(DOWN);
                break;
            case KeyEvent.VK_LEFT:
                move(LEFT);
                break;
            case KeyEvent.VK_UP:
                move(UP);
                break;
        }
        if (!e.isShiftDown())
            moveOnce = true;

    }

    @Override
    public void keyReleased(KeyEvent e){
        int code = e.getKeyCode();
        switch (code){
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_UP:
                if (!moveOnce)
                    stop();
                break;
        }
    }

    @Override
    void select(){
        super.select();
        stop();
    }

    @Override
    void deselect(){
        super.deselect();
        stop();
    }

    @Override
    public synchronized void parseCommand(String[] command){
        switch (command[0].toLowerCase()){
            case "move":
                if (command.length != 2){
                    board.printToConsole("move command requires exactly one argument \n" +
                            "possible arguments: up down left right");
                    break;
                }
                switch (command[1].toLowerCase()){
                    case "up":
                        move(UP);
                        moveOnce = true;
                        break;
                    case "down":
                        move(DOWN);
                        moveOnce = true;
                        break;
                    case "left":
                        move(LEFT);
                        moveOnce = true;
                        break;
                    case "right":
                        move(RIGHT);
                        moveOnce = true;
                        break;
                    default:
                        board.printToConsole("incorrect argument " + command[1] + " for move " +
                                "possible arguments: up down left right");
                }
                break;
            default:
                super.parseCommand(command);
        }
    }

    public void move(int dir){
        moveLock.lock();
        try{
            if (dir == CENTER){
                stop();
                return;
            }
            direction = dir;
            setImage(images[direction]);
            moving = true;
        }finally{
            moveLock.unlock();
        }
    }

    public void stop(){
        moveLock.lock();
        try{
            direction = CENTER;
            setImage(images[CENTER]);
            moving = false;
            moveOnce = false;
        }finally{
            moveLock.unlock();
        }
    }



}
