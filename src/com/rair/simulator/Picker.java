package com.rair.simulator;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by gavin on 7/12/17.
 */
public class Picker extends Pusher{
    boolean picking;
    List<Pickup> inventory;
    public Picker (int row, int col, Board board){
        super(row, col, board);
        picking = false;
        inventory = new ArrayList<>(2);
    }

    /**
     * Pick up a pickup on the current cell and add it to the inventory, removing it from the board.
     * @return True if an item was added to the inventory, false otherwise.
     */
    private boolean pickUp(){
        Cell c = board.getCell(getRow(),getCol());
        if (c.containsOccupantOfType(Pickup.class)){
            for (Sprite s: c.getOccupants()){
                if (s instanceof Pickup){
                    if (board.removeSprite(s)){
                        inventory.add((Pickup)s);
                    }else{
                        LOGGER.log(Level.WARNING, "Picker at " + getFormattedPosition() + " was unable to pick up" +
                                " a pickup. \n This is likely a threading error.");
                    }
                    //Pick up the pickup
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Place an item from the inventory on to the current cell.
     * @param i the inventory index to place an item from.
     * @return True if an item was removed from the inventory false otherwise.
     * @throws IndexOutOfBoundsException if the passed index is not valid
     */
    private boolean place(int i){
        Pickup p = inventory.remove(i);
        p.setLocation(getRow(), getCol());
        return board.addSprite(p);
    }

    @Override
    public void act(){
        if (picking){
            stop();
            if (!pickUp() && !inventory.isEmpty())
                place(0); //place the first pickup in the inventory
            picking = false;
        }
        super.act();
    }

    @Override
    public void keyReleased(KeyEvent e){
        if (e.getKeyCode() == KeyEvent.VK_SPACE)
            picking = true;
        else
            super.keyReleased(e);
    }

    @Override
    public void parseCommand(String[] args){
        if (args.length < 1)
            return;
        switch(args[0].toLowerCase()){
            case "pickup":
                picking = true;
                break;
            case "place":
                picking = false;
                if (!inventory.isEmpty())
                    place(0);
                else
                    board.printToConsole("No items to place.");
                break;
            default:
                super.parseCommand(args);
        }
    }
}
