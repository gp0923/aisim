package com.rair.simulator;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;


/**
 * A sprite is the base object that can be shown on the board
 * It has no interactions with the board aside from appearing as an image.
 */
public class Sprite implements Commons {
    protected static final Logger LOGGER = Logger.getLogger( Sprite.class.getName() );
    private int row;
    private int col;
    private boolean vis;
    private Image image;
    protected final Board board;
    private boolean selected;

    /**
     * Simple constructor for a sprite
     * @param row the row on the board the sprite will start in
     * @param col the column on the board the sprite will start in
     */
    public Sprite(int row, int col, Board board) {
        LOGGER.setLevel(LOG_LEVEL);
        this.row = row;
        this.col = col;
        this.board = board;
        vis = true;
        selected = false;
        initImages();
        LOGGER.log(Level.INFO, this.getClass().getSimpleName() + " Initialized at " + getFormattedPosition());
    }

    protected void initImages(){
        loadImage("resources/simulator/sprite_default.png");
    }

    /**
     * Change the image of the sprite
     * The image will be scaled to fit within the constraints given in the
     * commons file.
     * The aspect ratio of the image will be maintained when scaling.
     * .png images have been tested, .jpeg and .gif images should also work.
     * @param imageName The file location of the image.
     */
    public void loadImage(String imageName) {

        ImageIcon ii = new ImageIcon(imageName);
        setImage(ii.getImage());
    }


    public Image getImage() {
        return image;
    }

    /**
     * Change the image of the sprite
     * The image will be scaled down to fit within the constraints given in the
     * {@link Commons} interface, if necessary.
     * The aspect ratio of the image will be maintained when scaling.
     * Use {@link #loadImage(String) loadImage} to set the image from a file;
     * @param image The image to set.
     */
    public void setImage(Image image) {
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        if (w > SPRITE_WIDTH || h > SPRITE_HEIGHT){
            double ratio = Math.min(SPRITE_WIDTH/(double) w,
                    SPRITE_HEIGHT/(double) h);
            int width = (int) (w * ratio);
            int height = (int) (h * ratio);
            this.image = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        }
        else{
            this.image = image;
        }

    }

    /**
     * This method is called by the Board every clock tick.
     * Does nothing by default. Should be overridden by subclasses.
     */
    void act(){}

    /**
     * Called when this sprite is currently selected on the board and a key is pressed
     * Does nothing by default, override in subclasses.
     * @param e the key event
     */
    void keyPressed(KeyEvent e){}

    /**
     * Called when this sprite is currently selected on the board and a key is released
     * Does nothing by default, override in subclasses.
     * @param e the key event
     */
    void keyReleased(KeyEvent e){}

    /**
     * Called when this sprite is selected by the user
     * Sets selected to true by default.
     */
    void select(){
        selected = true;
    }

    /**
     * called when this sprite is no longer selected
     * Sets selected to false by default.
     */
    void deselect(){
        selected = false;
    }

    /**
     * Return true if the sprite is the currently selected sprite
     * @return
     */
    boolean isSelected() {return  selected;}

    /**
     * Parse a passed command,
     * called automatically by {@link Board}
     * @param command A string representation of the command
     */
    synchronized void parseCommand(String command[]){
        LOGGER.log(Level.FINER, getClass().getSimpleName() + " at (" + getRow() + ", " +
                getCol() + ") received command " + command[0] + " with " + (command.length - 1) + " arguments");
    }

    int getImageWidth() { return  image.getWidth(null); }
    int getImageHeight() { return  image.getHeight(null); }
    int getRow() {
        return row;
    }
    int getCol() {
        return col;
    }
    void setLocation(int row, int col){
        this.row = row;
        this.col = col;
    }
    boolean isVisible() {
        return vis;
    }
    public void setVisible(Boolean visible) {
        vis = visible;
    }

    public String getFormattedPosition(){
        return "(" + getRow() + "," + getCol() + ")";
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() + " (" + getRow() + "," + getCol() + ")";
    }
}

