package com.rair.simulator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A dynamic right-click menu to show avalible actions for a given cell and its sprites.
 */
public class DynamicMenu extends JPopupMenu {

    public DynamicMenu(){
        super();
        JMenuItem menuItem = new JMenuItem("Test Item");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Clicked: " + e.getActionCommand());
            }
        });
        add(menuItem);

    }
}
