package com.rair.simulator;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.*;

/**
 * A board representation for the AI simulator
 * Board acts as the base for the UI and contains all Sprites and other UI objects
 */

public class Board extends JPanel implements Runnable, Commons{
    /**
     * The logger for the board
     */
    private static final Logger LOGGER = Logger.getLogger( Board.class.getName() );

    private int width;
    private int height;
    private List<Sprite> sprites;
    private ReadWriteLock spriteAccessLock;
    private List<Cell> cells;
    private Cell selectedCell;
    private List<Ambient> ambients;
    private ReadWriteLock ambientAccessLock;
    private ReadWriteLock selectCellLock;
    private Sprite selectedSprite;
    private ReadWriteLock selectSpriteLock;
    private Console console;
    private int xOffset;
    private int yOffset;
    private Thread animator;
    private Boolean paused = false;
    //private DynamicMenu menu; TODO determine if the popupmenu should be stored locally
    /**
     * Default Constructor for Board
     */
    public Board() {
        LOGGER.setLevel(LOG_LEVEL);
        setLayout(new GridBagLayout());
        addKeyListener(new KAdapter());
        setFocusable(true);
        initWorld();
        initWorldSetup();
        setDoubleBuffered(true);
        LOGGER.log(Level.INFO, "Board successfully initialized");

    }

    /**
     * Initialize the class variables and set up the board and clock cycle
     * This should only be called by the constructor
     */
    private void initWorld() {
        width = CELL_LENGTH * BOARD_COLUMNS;
        height = CELL_LENGTH * BOARD_ROWS;
        sprites  = new ArrayList<>();
        spriteAccessLock = new ReentrantReadWriteLock();
        cells = new ArrayList<>(BOARD_ROWS*BOARD_COLUMNS);
        ambients  = new ArrayList<>(2);
        ambientAccessLock = new ReentrantReadWriteLock();
        selectedCell = null;
        selectCellLock = new ReentrantReadWriteLock();
        selectedSprite = null;
        selectSpriteLock = new ReentrantReadWriteLock();
        xOffset = (getWidth() - (BOARD_COLUMNS * CELL_LENGTH)) / 2;
        yOffset = (getHeight() - (BOARD_ROWS * CELL_LENGTH)) / 2;
        //menu = new DynamicMenu();

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        //Create the grid for the world by adding cells
        for (int row = 0; row < BOARD_ROWS; row++) {
            for (int col = 0; col < BOARD_COLUMNS; col++) {
                gbc.gridx = col;
                gbc.gridy = row;

                Cell cellPane = new Cell(this, row, col);
                Border border = new MatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY);

                cellPane.setBorder(border);
                add(cellPane, gbc);
                cells.add(cellPane);
            }
        }

        //Add the console to the grid
        gbc.gridx = 0;
        gbc.gridy = BOARD_ROWS;
        gbc.gridwidth = BOARD_COLUMNS;
        console = new Console(this);
        add(console, gbc);

        if (animator == null) {

            animator = new Thread(this);
            animator.start();
        }
    }

    /**
     * Initialize any sprites or changes to the default look of the world
     * that should be there when the world is initially loaded.
     */
    private void initWorldSetup(){
        addSprite(new Picker(BOARD_ROWS/2, BOARD_COLUMNS/2, this));
        addSprite(new Sprite(BOARD_ROWS/3, BOARD_COLUMNS/3, this));
        addSprite(new AniSprite((int) (BOARD_ROWS/1.5), (int) (BOARD_COLUMNS/1.5), this));
        addSprite(new Pickup(BOARD_ROWS/2, (int) (BOARD_COLUMNS/1.5), this));
        addSprite(new Blocker((int) (BOARD_ROWS/1.5), (int) (BOARD_COLUMNS/2), this));
        addSprite(new Pusher((int) (BOARD_ROWS/1.5), (int) (BOARD_COLUMNS/3), this));

//        addSprite(new Actor((int) (BOARD_ROWS/2.5), (int) (BOARD_COLUMNS/2.5-1), this));
//        addSprite(new Actor((int) (BOARD_ROWS/2.5 + 1), (int) (BOARD_COLUMNS/2.5-1), this));
//        addSprite(new Actor((int) (BOARD_ROWS/2.5 - 1), (int) (BOARD_COLUMNS/2.5-1), this));
//
//        Sprite table = new Sprite(BOARD_ROWS/2-1, BOARD_COLUMNS/2-2, this);
//        table.loadImage("resources/simulator/table_tl.png");
//        addSprite(table);
//        table = new Sprite(BOARD_ROWS/2, BOARD_COLUMNS/2-2, this);
//        table.loadImage("resources/simulator/table_bl.png");
//        addSprite(table);
//        table = new Sprite(BOARD_ROWS/2-1, BOARD_COLUMNS/2+1, this);
//        table.loadImage("resources/simulator/table_tr.png");
//        addSprite(table);
//        table = new Sprite(BOARD_ROWS/2, BOARD_COLUMNS/2+1, this);
//        table.loadImage("resources/simulator/table_br.png");
//        addSprite(table);
//        table = new Sprite(BOARD_ROWS/2-1, BOARD_COLUMNS/2, this);
//        table.loadImage("resources/simulator/table_top.png");
//        addSprite(table);
//        table = new Sprite(BOARD_ROWS/2, BOARD_COLUMNS/2, this);
//        table.loadImage("resources/simulator/table_bottom.png");
//        addSprite(table);
//        table = new Sprite(BOARD_ROWS/2-1, BOARD_COLUMNS/2-1, this);
//        table.loadImage("resources/simulator/table_top.png");
//        addSprite(table);
//        table = new Sprite(BOARD_ROWS/2, BOARD_COLUMNS/2-1, this);
//        table.loadImage("resources/simulator/table_bottom.png");
//        addSprite(table);
//        Sprite brings = new Sprite(BOARD_ROWS/2+1, BOARD_COLUMNS/2, this);
//        brings.loadImage("resources/simulator/bringsjord.jpg");
//        addSprite(brings);

    }

    /**
     * Get the preferred width of the board (not the component width)
     * @return the width of the board in pixels
     */
    public int getBoardWidth() { return this.width; }

    /**
     * Get the preferred height of the board (not the component height)
     * @return the height of the board in pixels
     */
    public int getBoardHeight() {
        return this.height;
    }


    /**
     * Set the given cell as the currently selected cell
     * The cell will be given a border to represent that it is selected
     * The passed cell must be on the board
     * @param c The cell to set as the selected cell
     */
    public void setSelectedCell(Cell c){
        selectCellLock.writeLock().lock();
        try{
            if (! cells.contains(c))
                return;
            clearSelectedCell();
            selectedCell = c;
            c.setBorder(BorderFactory.createLineBorder(HIGHLIGHT_COLOR, CELL_LENGTH/32));
            LOGGER.log(Level.FINE, "Selected: " + selectedCell);
            Sprite[] occ = selectedCell.getOccupants();
            if (occ != null) {
                LOGGER.log(Level.FINER, occ.toString());
            }
        }finally{
            selectCellLock.writeLock().unlock();
        }
    }

    /**
     * Clear the currently selected cell, removing its border.
     * May block if other threads are using this method or
     * setSelectedCell.
     */
    public void clearSelectedCell(){
        selectCellLock.writeLock().lock();
        try{
            if (selectedCell != null){
                selectedCell.setBorder(new MatteBorder(1, 1, 1 , 1,Color.LIGHT_GRAY));
                selectedCell = null;
            }
            LOGGER.log(Level.FINER, "Selected cell cleared");
        }finally{
            selectCellLock.writeLock().unlock();
        }

    }

    /**
     * Add a sprite to the grid. The initial position is determined by the row and column set in the sprite.
     * The sprite's coordinates must be a valid position on the grid.
     * @param s the sprite to add
     * @return true if the sprite was successfully added, false if not.
     */
    public boolean addSprite(Sprite s){
        spriteAccessLock.writeLock().lock();
        try{
            if (isOnBoard(s)){
                getCell(s.getRow(), s.getCol()).addOccupant(s);
                sprites.add(s);
                s.setVisible(true);
                LOGGER.log(Level.FINE,"Added sprite " + s.getClass().getSimpleName() +
                        " at position (" + s.getRow() + "," + s.getCol() + ")");
                return true;
            }
            return false;
        }finally{
            spriteAccessLock.writeLock().unlock();
        }

    }

    /**
     * Change the position of a sprite on the grid. The new position must be valid on the grid.
     * @param s The sprite to move, it should exist on the grid.
     * @param newRow The row to move the sprite to.
     * @param newCol The column to move te sprite to.
     * @return True if the sprite was successfully moved, false otherwise.
     */
    public boolean moveSprite(Sprite s, int newRow, int newCol) {
        spriteAccessLock.writeLock().lock();
        try{
            if (isOnBoard(s) && isOnBoard(newRow, newCol)){
                getCell(s.getRow(), s.getCol()).removeOccupant(s);
                s.setLocation(newRow, newCol);
                if (getCell(newRow, newCol).addOccupant(s)){
                    LOGGER.log(Level.FINER, "Sprite " + s + " moved to (" + newRow + ", " + newCol + ")");
                    return true;
                }else{
                    LOGGER.log(Level.WARNING, "Failed to move sprite " + s + " to (" + newRow + ", " + newCol + ")\n" +
                            "It has been removed from the board");
                }
            }
            return false;
        }finally{
            spriteAccessLock.writeLock().unlock();
        }

    }

    /**
     * Remove a sprite from the board.
     * @param s The sprite to remove from the board.
     * @return True if the sprite was removed, false otherwise.
     */
    public boolean removeSprite(Sprite s){
        spriteAccessLock.writeLock().lock();
        try{
            if (sprites.contains(s)){
                if (sprites.remove(s)){
                    boolean ret = getCell(s.getRow(), s.getCol()).removeOccupant(s);
                    if (ret){
                        if (s.isSelected()){
                            setSelectedSprite(null);
                        }
                        LOGGER.log(Level.FINE, "Sprite " + s + " removed");
                        s.setLocation(-1, -1);
                        s.setVisible(false);
                    }

                    return ret;
                }
            }
            return false;
        }finally {
            spriteAccessLock.writeLock().unlock();
        }

    }

    /**
     * Get the currently selected sprite
     * @return the currently selected sprite
     */
    public Sprite getSelectedSprite() {
        selectSpriteLock.readLock().lock();
        try{
            return selectedSprite;
        }finally {
            selectSpriteLock.readLock().unlock();
        }

    }

    /**
     * Set the currently selected sprite.
     * @param s
     */
    public void setSelectedSprite(Sprite s) {
        selectSpriteLock.writeLock().lock();
        try{
            if (selectedSprite != null)
                selectedSprite.deselect();
            this.selectedSprite = s;
            selectedSprite.select();
            LOGGER.log(Level.FINE, "Sprite " + selectedSprite +" is now selected");
        }finally {
            selectSpriteLock.writeLock().unlock();
        }
    }

    /**
     * Add an ambient object to the list
     * @param a The Ambient to add
     */
    public boolean addAmbient(Ambient a){
        ambientAccessLock.writeLock().lock();
        try{
            return ambients.add(a);
        }finally{
            ambientAccessLock.writeLock().unlock();
        }
    }

    /**
     * Add an ambient object to the list
     * @param a The Ambient to remove
     */
    public boolean removeAmbient(Ambient a){
        ambientAccessLock.writeLock().lock();
        try{
            return ambients.remove(a);
        }finally{
            ambientAccessLock.writeLock().unlock();
        }
    }

    /**
     * Add an ambient object to the list
     * @param a The Ambient to remove
     */
    public Ambient[] getAmbients(Ambient a){
        ambientAccessLock.readLock().lock();
        try{
            return ambients.toArray(new Ambient[0]);
        }finally{
            ambientAccessLock.readLock().unlock();
        }
    }

    /**
     * Parses the passed command, will pass command to sprites as necessary.
     * @param command A string representation of the command.
     */
    public void parseCommand(String command){
        command = command.trim();
        LOGGER.log(Level.FINE, "Command received: " + command);
        String[] arguments = command.split(" ");
        if (arguments.length < 1)
            return;
        switch (arguments[0].toLowerCase()){
            case ("select"):
                if (arguments.length != 3){
                    printToConsole("incorrect number of arguments for Select");
                    return;
                }
                try{
                    int row = Integer.parseInt(arguments[1]);
                    int col = Integer.parseInt(arguments[2]);
                    Cell c = getCell(row, col);
                    c.setSelected();
                    printToConsole("Selected " + selectedSprite);
                }catch (NumberFormatException e){
                    printToConsole("ERROR: Select command requires exactly 2 integers as arguments");
                }
                break;
            default:
                if (selectedSprite != null)
                    getSelectedSprite().parseCommand(arguments);
        }
    }

    /**
     * Print the passed string to the in-app console
     * @param s the string to print.
     */
    public void printToConsole(String s){
        LOGGER.log(Level.INFO, "Printed: " + s);
        console.println(s);
    }

    /**
     * Get the {@link Cell} at the given row and column.
     * @param row The row on the board
     * @param col The column on the board
     * @return The cell at the given location, null if that cell or location does not exist.
     */
    public Cell getCell(int row, int col){
        if (isOnBoard(row, col))
            return cells.get( (row*BOARD_COLUMNS) + col );
        return null;
    }

    /**
     * Returns true if the passed sprite has valid coordinates on the bridge.
     * Does not check if the sprite is actually visible on the board.
     * @param s The sprite
     * @return true if the sprite has valid coordinates, false otherwise
     */
    public boolean isOnBoard (Sprite s){
        if (s != null)
            return isOnBoard(s.getRow(), s.getCol());
        return false;
    }

    /**
     * Determines if the passe drow and column are on the board.
     * @param row The row to check.
     * @param col The column to check.
     * @return True if the row and column reference a cell on the board.
     */
    public boolean isOnBoard (int row, int col){
       return  col >= 0 && row >= 0 && col < BOARD_COLUMNS && row < BOARD_ROWS;
    }

    public void openPopupMenu(JPopupMenu m, int x, int y){
        m.show(this, x, y);
        //menu.setVisible(true);
    }



    /**
     * Draw the current state of the world
     * This is called by the paint method;
     * @param g The graphics for the board
     */
    private void drawWorld(Graphics g) {
        LOGGER.log(Level.FINEST, "Drawing world");
        //Graphics2D g2d = (Graphics2D) g.create();
        //g2d.dispose();
        for (Sprite s: sprites){
            //Sprites become invisible once they are off the grid
            if (!isOnBoard(s))
                s.setVisible(false);
            //Draw sprites that are visible
            if (s.isVisible() && s != getSelectedSprite()){
                int spWOffset = (CELL_LENGTH - s.getImageWidth())/2;
                int spHOffset = (CELL_LENGTH - s.getImageHeight())/2;
                g.drawImage(s.getImage(), s.getCol()*CELL_LENGTH + spWOffset, s.getRow()*CELL_LENGTH + spHOffset,
                        this);
            }
        }
        Sprite s = getSelectedSprite();
        if (s == null)
            return;
        int spWOffset = (CELL_LENGTH - s.getImageWidth())/2;
        int spHOffset = (CELL_LENGTH - s.getImageHeight())/2;
        g.drawImage(s.getImage(), s.getCol()*CELL_LENGTH + spWOffset, s.getRow()*CELL_LENGTH + spHOffset,
                this);
    }

    /**
     * Update all sprites, ambients and variables that change over time
     * Called by the animation thread.
     */
    private void actionCycle() {
        LOGGER.log(Level.FINEST, "Cycling actions");
        for (Sprite s: sprites.toArray(new Sprite[0])){
            if (isOnBoard(s))
                s.act();
        }
        for (Ambient a: ambients){
            a.act();
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        drawWorld(g);
    }

    @Override
    public void run() {
        LOGGER.log(Level.INFO, "Running animation thread");
        long beforeTime, timeDiff, sleep;

        beforeTime = System.currentTimeMillis();

        while (!paused) {

            repaint();
            actionCycle();

            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;

            if (sleep < 0) {
                sleep = 2;
            }

            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "Animation thread interrupted");
            }

            beforeTime = System.currentTimeMillis();
        }
    }

    /**
     * KeyAdapter class that passes keystrokes to the selected sprite
     */
    class KAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            if (selectedSprite != null)
                selectedSprite.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (selectedSprite != null)
                selectedSprite.keyReleased(e);
        }
    }
}
