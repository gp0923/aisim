package com.rair.simulator;

/**
 * A simple sprite that overrides the default sprite image
 */
public class Blocker extends Sprite {

    public Blocker(int row, int col, Board board) {
        super(row, col, board);

    }

    @Override
    protected void initImages(){
        loadImage("resources/simulator/blocker.png");
        if (getImage() == null)
            super.initImages();
    }
}
