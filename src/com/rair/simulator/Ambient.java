package com.rair.simulator;

/**
 * Abstract class to represent objects associated with the board, not a specific cell.
 */
public abstract class Ambient {
    protected final Board board;

    public Ambient(Board b){
        board = b;
    }

    /**
     * This method is called by the board after each animation tick
     */
    public abstract void act();
}
