package com.rair.simulator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * A sprite to showcase simple animation.
 */
public class AniSprite extends Sprite implements Commons{
    private Image[] images;
    int currentImage;
    //The total time to cycle through tall the images in ms.
    final int TOTAL_ANIM_TIME = 500;
    boolean running;

    public AniSprite(int row, int col, Board board){
        super(row, col, board);
        initImages();
        running = true;
    }

    @Override
    protected void initImages(){
        images = new Image[8];
        loadImage("resources/simulator/anisprite.png", 0);
        loadImage("resources/simulator/anisprite1.png", 1);
        loadImage("resources/simulator/anisprite2.png", 2);
        loadImage("resources/simulator/anisprite3.png", 3);
        loadImage("resources/simulator/anisprite4.png", 4);
        loadImage("resources/simulator/anisprite5.png", 5);
        loadImage("resources/simulator/anisprite6.png", 6);
        loadImage("resources/simulator/anisprite7.png", 7);

        setImage(images[0]);
        currentImage = 0;
    }

    public void loadImage(String imageName, int index) {

        ImageIcon ii = new ImageIcon(imageName);
        setImage(ii.getImage(), index);
    }

    public void setImage(Image image, int index) {
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        if (w > SPRITE_WIDTH || h > SPRITE_HEIGHT){
            double ratio = Math.min(SPRITE_WIDTH/(double) w,
                    SPRITE_HEIGHT/(double) h);
            int width = (int) (w * ratio);
            int height = (int) (h * ratio);
            images[index] = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        }
        else{
            images[index] = image;
        }
        currentImage = index;
    }

    @Override
    public void act(){
        if (running){
            currentImage += DELAY;
            if (currentImage >= TOTAL_ANIM_TIME)
                currentImage = 0;
            int imageToSet = ((((currentImage * 100) / TOTAL_ANIM_TIME) * images.length) / 100);
            setImage(images[imageToSet]);
        }
    }

    @Override
    public void keyReleased(KeyEvent e){
        if (e.getKeyCode() == KeyEvent.VK_SPACE)
            running = !running;
    }
}
