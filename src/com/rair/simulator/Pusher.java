package com.rair.simulator;

/**
 * A sprite that can move and push around blockers
 */
public class Pusher extends Mob implements Commons{
    public Pusher(int row, int col, Board board) {
        super(row, col, board);
    }

    @Override
    public void move(int dir){
        if (!push(dir) && !isBlocked(getCellInDirection(1, dir)))
            super.move(dir);
    }

    /**
     * Attempt to push a blocker or pusher in an adjacent cell.
     * Will move a blocker or pusher in the adjacent cell in direction dir one
     * cell in that direction iff the cell it would move into does not have a pusher or blocker
     * and is on the board.
     * @param dir the direction to try and push
     * @return true if successfully able to push a blocker
     */
    private boolean push(int dir){
        Cell target = getCellInDirection(1, dir);
        Cell destination = getCellInDirection(2, dir);

        if (target != null && destination != null && isBlocked(target) && !isBlocked(destination)){
            for (Sprite s: target.getOccupants()){
                if (s instanceof Blocker || s instanceof Pusher ){
                    return board.moveSprite(s, destination.getRow(), destination.getCol());
                }
            }
        }
        return false;
    }

    /**
     * Get the cell dis spaces away in direction dir
     * @param dis the number of spaces away from the current cell the sprite is on.
     *            0 will return the current cell. Can be negative
     * @param dir The direction to get the new cell, See the commons interface for direction constants.
     * @return The cell specified, null if it does not exist.
     */
    private Cell getCellInDirection(int dis, int dir){
        int pushRowDiff = 0;
        int pushColDiff = 0;

        switch (dir){
            case RIGHT:
                pushColDiff = dis;
                break;
            case DOWN:
                pushRowDiff = dis;
                break;
            case LEFT:
                pushColDiff = -dis;
                break;
            case UP:
                pushRowDiff = -dis;
                break;
        }
        if (!board.isOnBoard(getRow() + pushRowDiff, getCol() + pushColDiff))
            return  null;
        return board.getCell(getRow() + pushRowDiff, getCol() + pushColDiff);
    }

    /**
     * Checks if the passed cell has something blocking it
     * @param c the cell to check
     * @return True if c is null or c contains a blocker or pusher.
     */
    private static boolean isBlocked(Cell c){
        if (c == null)
            return true;
        if (c.containsOccupantOfType(Blocker.class) || c.containsOccupantOfType(Pusher.class))
            return true;
        return false;
    }

}
