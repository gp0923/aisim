package com.rair.simulator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

/**
 * A panel that makes up a square on the grid.
 * It acts as a listener and will send callbacks to the board.
 */
public class Cell extends JPanel implements Commons{
    protected static final Logger LOGGER = Logger.getLogger( Cell.class.getName() );
    private Board board;
    private Color defaultBackground;
    private final int row;
    private final int col;
    private List<Sprite> occupants;
    private ReadWriteLock occupantLock;

    public Cell (Board b, int row, int col){
        LOGGER.setLevel(LOG_LEVEL);
        board = b;
        this.row = row;
        this.col = col;
        occupants = null;
        occupantLock = new ReentrantReadWriteLock();
        setBackground(new Color(0,0,0, 0));
        defaultBackground = getBackground();

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(new Color(50, 50, 255, 100));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(defaultBackground);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                checkPopup(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                checkPopup(e);
            }

            @Override
            public void mouseClicked(MouseEvent e){
                if (!checkPopup(e)){
                    board.requestFocusInWindow();
                    setSelected();
                }
            }

            private boolean checkPopup(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    JPopupMenu pmenu = new DynamicMenu();
                    if (containsOccupant(board.getSelectedSprite())){

                    }else if (!isUnoccupied()){

                    }else{

                    }
                    board.openPopupMenu(pmenu, e.getX()+ getX(), e.getY() + getY());
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(CELL_LENGTH, CELL_LENGTH);
    }

    /**
     * Sets the current cell as the selected cell
     */
    public void setSelected(){
        board.setSelectedCell(this);
        if (!isUnoccupied()){
            Sprite s = board.getSelectedSprite();
            if (s != null && containsOccupant(s)){
                if (occupants.size() > 1)
                    board.setSelectedSprite(occupants.get((occupants.indexOf(s) + 1) % occupants.size()));
                else
                    return;
            }
            else
                board.setSelectedSprite(occupants.get(0));
        }
    }


    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    /**
     * Add a {@link Sprite} as an occupant of the cell
     * Sprite position must match the Cell's position on the board.
     * Null sprites will not be added.
     * @param s The {@link Sprite} to add.
     * @return True if the sprite was successfully added, false otherwise.
     */
    public boolean addOccupant(Sprite s){
        occupantLock.writeLock().lock();
        try{
            if (occupants == null)
                occupants = new ArrayList<>(1);
            return occupants.add(s);
        }finally{
            occupantLock.writeLock().unlock();
        }
    }

    /**
     * Remove the first instance of an occupant from the cell
     * @param s The occupant to remove
     * @return True if occupant was removed, false otherwise
     */
    public boolean removeOccupant(Sprite s){
        occupantLock.writeLock().lock();
        try{
            if (occupants == null)
                return false;
            boolean ret = occupants.remove(s);
            if (occupants.isEmpty())
                occupants = null;
            return ret;
        }finally{
            occupantLock.writeLock().unlock();
        }
    }

    public boolean containsOccupant(Sprite s){
        occupantLock.readLock().lock();
        try{
        if (occupants == null)
            return false;
        return occupants.contains(s);
        }finally{
            occupantLock.readLock().unlock();
        }
    }

    /**
     * Returns true if the cell contains an occupant that is of the passed class
     * or a subclass of the passed class
     * @param c the class to check
     * @return true if an occupant of the type is found. False otherwise.
     */
    public boolean containsOccupantOfType(Class c){
        occupantLock.readLock().lock();
        try{
            if (occupants == null)
                return false;
            for (Sprite s: occupants)
                if (c.isInstance(s))
                    return true;
            return false;
        }finally{
            occupantLock.readLock().unlock();
        }
    }

    /**
     *
     * @return True if the cell contains no sprites, false otherwise
     */
    public boolean isUnoccupied(){
        occupantLock.readLock().lock();
        try{
            return (occupants == null || occupants.isEmpty());
        }finally{
            occupantLock.readLock().unlock();
        }
    }

    /**
     * Get a list of occupants in the cell
     * @return A list of occupants contained in the cell if the cell is empty, an empty list is returned
     */
    public Sprite[] getOccupants(){
        occupantLock.readLock().lock();
        try{
            if (occupants == null || isUnoccupied())
                return new Sprite[0];
            return occupants.toArray(new Sprite[0]);
        }finally{
            occupantLock.readLock().unlock();
        }
    }

    /**
     * Get the number of sprites associated with this cell
     * @return the number of sprites in the cell
     */
    public int numOccupants() {
        occupantLock.readLock().lock();
        try{
            if (occupants == null)
                return 0;
            return occupants.size();
        }finally{
            occupantLock.readLock().unlock();
        }
    }

    /**
     * Get the position of the cell in a string in the form (Row,Col)
     * @return a string containing the position of the cell in parenthetical form
     */
    public String getFormattedPosition(){
        return "(" + getRow() + "," + getCol() + ")";
    }

    @Override
    public String toString(){
        return ("Cell at "+ getFormattedPosition() + " with " + numOccupants() + " occupant(s).");
    }
}
