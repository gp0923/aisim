package com.rair.simulator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * A Panel that handles reading and writing from/to the in-app console.
 */

public class Console extends JPanel implements Commons{

    private JScrollPane pane;
    JTextArea output;
    Board board;

    public Console(Board b){
        board = b;
        initUI();
    }

    private void initUI() {

        output = new JTextArea(CELL_LENGTH / 8,CELL_LENGTH * BOARD_COLUMNS / 16);
        output.setLineWrap(true);
        output.setEditable(false);
        pane = new JScrollPane(output, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        JTextField input = new JTextField(20);
        input.addActionListener((ActionEvent e) -> {
            String s = input.getText();
            if (!s.equals("")) {
                sendCommand(s);
                input.setText("");
            }
        });

        setLayout(new BorderLayout());
        add(pane, BorderLayout.CENTER);
        add(input, BorderLayout.SOUTH);
        setFocusable(true);
    }


    /**
     * Send a command to the board
     * @param command the command to send,formatted as a string
     */
    private void sendCommand(String command){
        if (command != null && command != ""){
            println("Echo: " + command);
            board.parseCommand(command);
        }
    }

    public synchronized void println(String s){
        output.append(s + "\n");
        //repaint();
    }

}
