package com.rair.simulator;

import java.awt.*;

/**
 * Created by gavin on 7/27/17.
 */
public class Actor extends Mob{

    public Actor(int row, int col, Board board){
        super(row, col, board);
    }

    @Override
    protected void initImages() {
        images = new Image[5];
        loadImage("resources/simulator/actor_up.png", CENTER);
        loadImage("resources/simulator/actor_right.png", RIGHT);
        loadImage("resources/simulator/actor_down.png", DOWN);
        loadImage("resources/simulator/actor_left.png", LEFT);
        loadImage("resources/simulator/actor_up.png", UP);
        setImage(images[CENTER]);
    }

    @Override
    public void stop() {
        setImage(images[direction], CENTER);
        super.stop();
    }
}
