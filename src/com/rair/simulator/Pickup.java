package com.rair.simulator;


public class Pickup extends Sprite {
    public Pickup(int row, int col, Board board) {
        super(row, col, board);
        loadImage("resources/simulator/pickup.png");
    }
}
